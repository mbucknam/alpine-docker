#!/bin/bash


#
#
# Another way of running with xvfb-run but more complicated
#
#     As root start xvfb in daemon mode
#
#start-stop-daemon --start --quiet --pidfile /tmp/selenium_xvfb_99.pid --make-pidfile --background --exec /usr/bin/Xvfb -- :99 -ac -screen 0 1280x1024x16
#
#     As root export display to env and start the selenium server as selenium
#
#su - selenium -c 'export DISPLAY=:99 /usr/bin/xvfb-run java -Djava.security.egd=file:///dev/urandom -jar /opt/selenium/selenium-server-standalone.jar -port 4444'
#
#xvfb-run java -Djava.security.egd=file:///dev/urandom -jar selenium-server-standalone.jar -port 4444 &
#
#
# Comment this out and start top to start vm for debugging
#
#     Uncomment top at the end -- it just keeps the machine from
#     exiting so you can exec a bash shell as :
#
#         docker exec -i -t ${container_id} /bin/bash
#
#     To attach and tail the output :
#
#         docker attach ${container_id}
#
#
#

function shutdown {
    echo "shutting down selenium server.."
    kill -s SIGTERM $SERVER_PID
    wait $SERVER_PID
    echo "shutdown complete"
}

SCREEN_WIDTH=1920
SCREEN_HEIGHT=1080
SCREEN_DEPTH=8

export GEOMETRY="$SCREEN_WIDTH""x""$SCREEN_HEIGHT""x""$SCREEN_DEPTH"
export JAVA_OPTS="-Djava.security.egd=file:///dev/urandom"

if [ ! -z "$SE_OPTS" ]; then
  echo "appending selenium options: ${SE_OPTS}"
fi

echo "starting selenium server with configuration :"
echo "JAVA_OPTS = $JAVA_OPTS"
echo "GEOMETRY = $GEOMETRY"

xvfb-run --server-args="-screen 0 $GEOMETRY -ac +extension RANDR" \
    java $JAVA_OPTS -jar selenium-server-standalone.jar \
        ${SE_OPTS} &
    SERVER_PID=$!

trap shutdown SIGTERM SIGINT
wait $SERVER_PID
