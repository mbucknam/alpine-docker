#!/bin/sh

#
#
#
# Comment the rest of this script and start top to start vm for debugging
#
#     Uncomment top at the end -- it just keeps the machine from
#     exiting so you can exec a bash shell as :
#
#         docker exec -i -t ${container_id} /bin/bash
#
#     To attach and tail the output :
#
#         docker attach ${container_id}
#
# As root start dbus for exported display to host
start-stop-daemon --start --pidfile /var/run/dbus.pid --exec /usr/bin/dbus-daemon -- --system

# As root start xvfb in daemon mode
start-stop-daemon --start --quiet --pidfile /tmp/selenium_xvfb_99.pid --make-pidfile --background --exec /usr/bin/Xvfb -- :99 -ac -screen 0 1280x1024x16

# As root export display to env and start chrome as developer
#
su - developer -c 'export DISPLAY=:99; chromium-browser --no-sandbox --'

# Only to keep machine running for debugging
#top

