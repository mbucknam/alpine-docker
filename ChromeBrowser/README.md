#       Alpine Linux Chrome Browser for Selenium Server - 2.53.1

Alpine Linux Chrome Browser for Selenium Server running 2.53.1

##      Reference

Running GUI apps with Docker

    http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/

Selenium HQ

    https://github.com/SeleniumHQ/docker-selenium

##      How to use this image

This image will attempt to share the running user's X11 socket with the container and use it directly.

By default the uid and gid are 1002. Create a user with those values and run the container as that user with this
command :

```
$ docker exec -it fivebucks-alpine/chrome-browser:2.53.1 export DISPLAY=:99; chromium-browser --no-sandbox --disable-gpu
```
