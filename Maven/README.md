#       Alpine Linux Service Registry version 

Alpine Linux Service Registry running version 

##      How to use this image


```
$ docker run -d -P fivebucks-alpine/service-registry:
```


You can acquire the port(s) that Service Registry is listening on by running:

``` bash
$ docker port <container-name|container-id> 8080
#=> 0.0.0.0:49338
```
