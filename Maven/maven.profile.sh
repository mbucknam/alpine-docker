export JAVA_HOME=/usr/lib/jvm/default-jvm
export M2_HOME=/$APP_DIR/default-mvn \
export PATH=$PATH:$JAVA_HOME/bin:$M2_HOME/bin
export JAVA_OPTS="-Djava.security.egd=file:///dev/urandom"
