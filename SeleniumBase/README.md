#       Alpine Linux Selenium Server - 2.53.1

Alpine Linux Selenium Server running 2.53.1

##      Reference

Selenium HQ

    https://github.com/SeleniumHQ/docker-selenium

##      How to use this image

```
$ docker run -d -P fivebucks-alpine/selenium-server:2.53.1
```

You can acquire the port(s) that Selenium Server is listening on by running:

``` bash
$ docker port <container-name|container-id> 4444
#=> 0.0.0.0:49338
```
