#       Alpine Linux Consul Server version ##VERSION##

Alpine Linux Consul Server version ##VERSION##

##      Reference

Hashicorp Official Container

    https://github.com/hashicorp/docker-consul
    https://www.hashicorp.com/blog/official-consul-docker-image.html

##      How to use this image

``` bash

docker run -d -P fivebucks-alpine/consul:##VERSION##

```


Create a new container

- On a custom network named `test-network`
- Using volumes from a storage container named `test-storage`
- Alias name `consul`

    The custom network must already exist
    The storage container must already exist

``` bash

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name consul \
    fivebucks-alpine/consul:##VERSION##

```

Run the container just created

``` bash

docker start consul

```

You can acquire the port(s) that Consul is listening on by running:

``` bash

docker port <container-name|container-id> 8500
#=> 0.0.0.0:49338

```
