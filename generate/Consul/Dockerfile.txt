MAINTAINER Matt Bucknam <mbucknam@gmail.com>

# Add environment variables
ENV APP_USER=consul \
    APP_DIR=/opt/consul \
    APP_VERSION=0.7.0

# Add user
RUN adduser -D ${APP_USER}

# Copy local files
COPY consul-simple.sh ${APP_DIR}/consul.sh

RUN apk update && apk add \
    ca-certificates  \
    wget \
    && \
    update-ca-certificates

RUN mkdir -p /tmp/build \
    && \
    cd /tmp/build       \
    && \
    wget https://releases.hashicorp.com/consul/${APP_VERSION}/consul_${APP_VERSION}_linux_amd64.zip \
    && \
    unzip -d ${APP_DIR} consul_${APP_VERSION}_linux_amd64.zip \
    && \
    ln -s ${APP_DIR}/consul /usr/local/bin/consul \
    && \
    apk del wget \
    && \
    rm -rf /var/cache/apk/* \
    && \
    cd /tmp             \
    && \
    rm -rf /tmp/build

# The data dir is used by Consul to store state. The agent will be started
# with config as the configuration directory so you can add additional
# config files in that location.
RUN mkdir -p ${APP_DIR}/data   \
    && \
    mkdir -p ${APP_DIR}/config \
    && \
    chown -R ${APP_USER}:${APP_USER} ${APP_DIR} \
    && \
    chmod a+x ${APP_DIR}/*.sh

# Expose the consul data directory as a volume since there's mutable state in there.
VOLUME ${APP_DIR}/data

# Server RPC is used for communication between Consul clients and servers for internal
# request forwarding.
EXPOSE 8300

# Serf LAN and WAN (WAN is used only by Consul servers) are used for gossip between
# Consul agents. LAN is within the datacenter and WAN is between just the Consul
# servers in all datacenters.
EXPOSE 8301 8301/udp 8302 8302/udp

# CLI, HTTP, and DNS (both TCP and UDP) are the primary interfaces that applications
# use to interact with Consul.
EXPOSE 8400 8500 8600 8600/udp

WORKDIR ${APP_DIR}
USER ${APP_USER}
CMD ["./consul.sh"]
