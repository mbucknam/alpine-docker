#       Alpine Linux Selenium Server - ##VERSION##

Alpine Linux Selenium Server running ##VERSION##

##      Reference

Selenium HQ

    https://github.com/SeleniumHQ/docker-selenium

##      How to use this image

```
$ docker run -d -P fivebucks-alpine/selenium-server:##VERSION##
```

You can acquire the port(s) that Selenium Server is listening on by running:

``` bash
$ docker port <container-name|container-id> 4444
#=> 0.0.0.0:49338
```
