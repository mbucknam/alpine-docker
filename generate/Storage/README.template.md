#       Alpine Linux Storage version ##VERSION##

Alpine Linux Storage running version ##VERSION##

##      How to use this image

- Create a data volume container to be mounted by the test containers

``` bash

docker create -it -v /data --name test-storage fivebucks-alpine/base:##VERSION## /bin/bash

```

- Start the test-storage container

``` bash

docker start test-storage

```

- Connect to the test-storage container

``` bash

docker exec -it test-storage /bin/bash

```
