#!/bin/bash
FOLDER=../../$1
THIS_VERSION=$2
BASE=$3
VERSION=$4

echo "# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" > $FOLDER/Dockerfile
echo "# NOTE: DO *NOT* EDIT THIS FILE.  IT IS GENERATED." >> $FOLDER/Dockerfile
echo "# PLEASE UPDATE Dockerfile.txt INSTEAD OF THIS FILE" >> $FOLDER/Dockerfile
echo "# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $FOLDER/Dockerfile
echo FROM fivebucks-alpine/$BASE:$VERSION >> $FOLDER/Dockerfile
cat ./Dockerfile.txt >> $FOLDER/Dockerfile

cat ./README.template.md \
  | sed "s/##VERSION##/$VERSION/" \
  | sed "s/##FOLDER##/$1/" > $FOLDER/README.md

cat ./README-short.template.txt \
  | sed "s/##VERSION##/$VERSION/" > $FOLDER/README-short.txt
