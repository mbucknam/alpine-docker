#       Alpine Linux Selenium Server - ##VERSION##

Alpine Linux Selenium Server running ##VERSION##

##      Reference

Selenium HQ

    https://github.com/SeleniumHQ/docker-selenium

##      How to use this image

Default

``` bash

docker run -d -P fivebucks-alpine/selenium-server:##VERSION##

```
Create a new container

- On a custom network named `test-network`
- Using volumes from a storage container named `test-storage`
- Alias name `selenium`

    The custom network must already exist
    The storage container must already exist

``` bash

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name selenium \
    fivebucks-alpine/selenium-server:##VERSION##

```

Run the container just created

``` bash

docker start selenium

```

You can acquire the port(s) that Selenium Server is listening on by running:

``` bash

docker port <container-name|container-id> 4444
#=> 0.0.0.0:49338

```
