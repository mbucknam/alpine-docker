#!/bin/bash
FOLDER=../../$1
SELENIUM_VERSION=$2
BASE=$3
VERSION=$4


echo "# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" > $FOLDER/Dockerfile
echo "# NOTE: DO *NOT* EDIT THIS FILE.  IT IS GENERATED." >> $FOLDER/Dockerfile
echo "# PLEASE UPDATE Dockerfile.txt INSTEAD OF THIS FILE" >> $FOLDER/Dockerfile
echo "# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $FOLDER/Dockerfile
echo FROM fivebucks-alpine/$BASE:$VERSION >> $FOLDER/Dockerfile
cat ./Dockerfile.txt >> $FOLDER/Dockerfile

MODE_LC=$(echo $MODE |  tr '[:upper:]' '[:lower:]')

cat ./README.template.md \
  | sed "s/##VERSION##/$SELENIUM_VERSION/" \
  | sed "s/##MODE_LC##/$MODE_LC/" \
  | sed "s/##BASE##/$BASE/" \
  | sed "s/##FOLDER##/$1/" > $FOLDER/README.md


cat ./README-short.template.txt \
  | sed "s/##VERSION##/$SELENIUM_VERSION/" > $FOLDER/README-short.txt
