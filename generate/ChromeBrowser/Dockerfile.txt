MAINTAINER Matt Bucknam <mbucknam@gmail.com>

# See :
#
#    http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/
#
# Start docker with this command :
#
#    docker exec -i -t 18 export DISPLAY=:99; chromium-browser --no-sandbox --disable-gpu
#
#
# Disable GPU because this image does not have support.
# No sandbox until kernel support becomes available for user namespaces
#

USER root

RUN apk update && apk add \
    dbus \

    && rm -rf /var/cache/apk/*

# Copy start script.
COPY chrome-browser.sh /usr/local/bin/chrome-browser.sh
RUN chmod a+x /usr/local/bin/chrome-browser.sh

# Replace 1000 with your user / group id
RUN export uid=1002 gid=1002 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

WORKDIR /home/developer
USER developer

ENTRYPOINT ["chrome-browser.sh"]
