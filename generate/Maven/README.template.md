#       Alpine Linux Service Registry version ##VERSION##

Alpine Linux Service Registry running version ##VERSION##

##      How to use this image


```
$ docker run -d -P fivebucks-alpine/service-registry:##VERSION##
```


You can acquire the port(s) that Service Registry is listening on by running:

``` bash
$ docker port <container-name|container-id> 8080
#=> 0.0.0.0:49338
```
