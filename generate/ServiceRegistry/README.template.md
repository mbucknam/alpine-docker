#       Alpine Linux Service Registry version ##VERSION##

Alpine Linux Service Registry running version ##VERSION##

##      How to use this image


``` bash

docker run -d -P fivebucks-alpine/service-registry:##VERSION##

```

Create a new container

- On a custom network named `test-network`
- Using volumes from a storage container named `test-storage`
- Alias name `registry`

    The custom network must already exist
    The storage container must already exist

``` bash

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name registry \
    fivebucks-alpine/service-registry:##VERSION##

```

Run the container just created

``` bash

docker start registry

```

You can acquire the port(s) that Service Registry is listening on by running:

``` bash

docker port <container-name|container-id> 8080
#=> 0.0.0.0:49338

```
