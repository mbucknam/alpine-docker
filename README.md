#       Alpine Docker Containers

Several different containers based on Alpine Linux. In most cases they are modeled after existing containers that use
other base OS implementations, resulting in significant reduction in size.

##      References

Example on Alpine

    https://github.com/SUNx2YCH/docker-alpine-selenium

SeleniumHQ Reference Containers

    https://github.com/SeleniumHQ/docker-selenium

Adding VNC to Alpine Container

    https://hub.docker.com/r/fernandosanchez/simplevnc/
    http://forum.alpinelinux.org/forum/general-discussion/solved-x11vnc-not-working-alpine33

Not sure why this link is important

    https://github.com/cjpetrus/alpine_webkit2png/blob/master/Dockerfile

##      Docker Commands

An assortment of random useful docker command lines

###     Connect To Running Container

Attach so that CTL-C will release :

    docker attach --sig-proxy=false ${container-id}

Connect to bash shell :

    docker exec -it 70 /bin/bash

Stop nicely

    docker stop 70

Stop meanly

    docker kill 70

###        Network

Inspect a network and list running containers

    docker network inspect test-network

###     The "Bleach Bit"

Name inspired by a famous politician. Forcibly and thoroughly remove all local containers, images, and volumes

    # Must be run first because images are attached to containers
    docker rm $(docker ps -a -q)

    # Delete every Docker image
    docker rmi $(docker images -q)

    # Remove dangling volumes
    # docker volume rm $(docker volume ls -qf dangling=true)

    # handles no-op but Linux specific
    docker volume ls -qf dangling=true | xargs -r docker volume rm

    # Get any leftovers
    sudo rm -rf /var/lib/docker/image/devicemapper/imagedb/content/sha256/*


##        Docker Configuration

###        ENTRYPOINT or CMD

See these references for the differences between ENTRYPOINT and CMD

References :

    https://dmp.fabric8.io/#misc-startup

    http://stackoverflow.com/questions/21553353/what-is-the-difference-between-cmd-and-entrypoint-in-a-dockerfile


##      Command History (to be edited later for useful nuggets)

  808  docker run -it --rm --net host --cpuset-cpus 0 --memory 512mb -v /tmp/.X11-unix/:/tmp/.X11-unix/ -e DISPLAY=$DISPLAY -v $HOME/:/home/developer/ --device /dev/snd -v /dev/shm/:/dev/shm/ -v /usr/share/fonts/:/usr/share/fonts/ rodrigomiguele/chromium "$@"
  810  docker build -t bucknamm/selenium-base .
  812  docker build -t bucknamm/selenium-standalone .
  813  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  814  docker exec -i -t 70 /bin/bash
  815  docker ps
  816  docker kill 70
  817  docker build -t bucknamm/selenium-standalone .
  818  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  819  docker exec -i -t cd /bin/bash
  821  docker build -t bucknamm/selenium-standalone .
  822  docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  823  docker ps
  824  docker build -t bucknamm/selenium-standalone .
  825  docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  826  docker ps
  827  docker build -t bucknamm/selenium-standalone .
  828  docker ps
  829  docker kill 9d
  830  docker build -t bucknamm/selenium-standalone .
  833  docker build -t bucknamm/selenium-base .
  835  docker build -t bucknamm/selenium-standalone .
  837  docker build -t bucknamm/selenium-base .
  839  docker build -t bucknamm/selenium-standalone .
  840  docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  841  docker build -t bucknamm/selenium-standalone .
  842  docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  843  docker build -t bucknamm/selenium-standalone .
  844  docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  846  docker ps
  847  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bucknamm/selenium-standalone
  848  docker exec -i -t 18 /bin/bash
  849  docker exec -i -t 18 chromium-browser --no-sandbox --disable-gpu
  850  docker exec -i -t 18 export DISPLAY=:99; chromium-browser --no-sandbox --disable-gpu
  852  docker exec -i -t 18 chromium-browser --disable-gpu
  853  docker exec -i -t 18 /bin/bash
  854  docker exec -i -t 18 chromium-browser --disable-gpu
  855  docker exec -i -t 18 export DISPLAY=:99; chromium-browser --no-sandbox --disable-gpu
  856  docker exec -i -t 18 export DISPLAY=:99; xvfb-run chromium-browser --no-sandbox --disable-gpu
  858  docker exec -i -t 18 export DISPLAY=:99; xvfb-run chromium-browser --no-sandbox --disable-gpu
  859  docker exec -i -t 18 export DISPLAY=:99; chromium-browser --no-sandbox --disable-gpu
  860  docker exec -i -t 18 xfvb-run chromium-browser --no-sandbox --disable-gpu
  861  docker exec -i -t 18 /usr/bin/xfvb-run chromium-browser --no-sandbox --disable-gpu
  862  docker exec -i -t 18 /bin/bash
  863  docker ps
  864  docker kill 184
  865  vim /home/bucknamm/bin/docker-scrub-containers-and-images.sh
  866  chmod 755 /home/bucknamm/bin/docker-scrub-containers-and-images.sh
  867  docker-scrub-containers-and-images.sh
  869  docker build -t bucknamm/selenium-base .
  870  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  871  docker attach d32
  872  docker ps
  873  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  874  docker attach d32
  875  docker attach 57
  876  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  877  docker exec -i -t 67 /bin/bash
  878  docker ps
  879  docker kill 67
  880  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  881  docker ps
  882  docker attach 84
  883  docker build -t bucknamm/selenium-base .
  884  docker ps
  885  docker build -t bucknamm/selenium-base .
  886  docker
  887  docker ps
  888  docker stop 526
  889  docker start 526
  890  docker stop 526
  892  docker exec -i -t cd /bin/bash
  893  docker ps
  894  docker kill cd
  895  docker build -t bucknamm/selenium-standalone .
  896  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  897  docker exec -i -t 9d /bin/bash
  901  docker build -t bucknamm/selenium-standalone .
  902  docker ps
  903  docker kill 57
  904  docker build -t bucknamm/selenium-standalone .
  905  docker ps
  906  docker stop 84
  907  docker build -t bucknamm/selenium-standalone .
  908  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  909  docker exec -i -t 9b /bin/bash
  910  docker ps
  911  docker stop 9b
  912  docker-scrub-containers-and-images.sh
  913  sudo docker-scrub-containers-and-images.sh
  914  sudo /home/bucknamm/bin/docker-scrub-containers-and-images.sh
  916  vim ~/bin/docker-scrub-containers-and-images.sh
  917  docker-scrub-containers-and-images.sh
  918  docker build -t bucknamm/selenium-standalone .
  919  docker run -d -p 4444:4444 -v /dev/shm:/dev/shm bucknamm/selenium-standalone
  920  docker attach 526
  921  docker run -d -p 4444:4444 -p 5900:5900 selenium/standalone-chrome-debug:2.53.1
  922  docker -version
  923  docker -v
  925  git clone git@github.com:SeleniumHQ/docker-selenium.git
  940  cd docker-bucknamm/
  946  docker ps -a
  949  git remote add origin git@gitlab.com:mbucknam/alpine-docker.git
  964  cd projects/va/docker/
  966  cd docker-bucknamm/
  968  cd alpine-docker/
  976  docker ps
  977  history docker exec
  978  history | grep 'docker exec'
  979  docker exec -it 09 /bin/bash
  981  docker ps -a
  982  history | grep docker
  983  docker run -d alpine-fivebucks/consul
  984  docker run -d fivebucks-alpine/consul
  986  docker run -d a0b
  988  docker run -d 8e25
  989  docker exec -it 5d /bin/bash
  990  docker ps
  991  docker kill 5d
  992  docker kill 09
  993  docker kill 94
  994  docker ps
  996  docker -d 732
  997  docker run -d 732
  999  docker run -d 65e
 1000  docker exec -it 4dd /bin/bash
 1001  docker ps
 1002  docker kill 45c
 1003  docker start 45c
