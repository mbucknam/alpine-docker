#!/bin/bash
#
#
#
#

echo "Starting Service Registry"

function shutdown {
    echo "shutting down selenium hub.."
    kill -s SIGTERM $APP_PID
    wait $APP_PID
    echo "shutdown complete"
}

JAVA_OPTS="-Djava.security.egd=file:///dev/urandom"

java $JAVA_OPTS  -jar $APP_DIR/service-registry.jar \
        ${SE_OPTS} &
    APP_PID=$!

trap shutdown SIGTERM SIGINT
wait $APP_PID
