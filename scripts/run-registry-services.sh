#!/bin/bash

docker create -it -v /data --name test-storage fivebucks-alpine/base:3.4 /bin/bash
docker start test-storage
docker exec -it test-storage chmod 777 /data
docker stop test-storage

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name consul \
    fivebucks-alpine/consul:0.7.0

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name registry \
    fivebucks-alpine/service-registry:0.1.0-SNAPSHOT

docker create -it \
    --network test-network \
    --volumes-from test-storage \
    --name selenium \
    fivebucks-alpine/selenium-server:2.53.1

docker start consul
docker start selenium
docker start registry

docker network inspect test-network