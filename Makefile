PREFIX := fivebucks-alpine
ALPINE_VERSION := $(or $(ALPINE_VERSION),'3.4')
MAVEN_VERSION := $(or $(MAVEN_VERSION),'3.3.9')
SELENIUM_VERSION := $(or $(SELENIUM_VERSION),'2.53.1')
CHROME_VERSION := $(or $(CHROME_VERSION),'2.53.1')
CONSUL_VERSION := $(or $(CONSUL_VERSION),'0.7.0')
SERVICE_REGISTRY_VERSION := $(or $(SERVICE_REGISTRY_VERSION),'0.1.0-SNAPSHOT')

PLATFORM := $(shell uname -s)
BUILD_ARGS := $(BUILD_ARGS)

print-%  : ; @echo $* = $($*)
	
all: storage maven selenium_base selenium_server chrome_browser consul service_registry

generate_all:	\
	generate_storage generate_maven generate_selenium_base generate_selenium_server generate_chrome_browser generate_consul generate_service_registry

build: all

print_vars:
	@echo PREFIX = $(PREFIX)
	@echo ALPINE_VERSION = $(ALPINE_VERSION)
	@echo MAVEN_VERSION = $(MAVEN_VERSION)
	@echo SELENIUM_VERSION = $(SELENIUM_VERSION)
	@echo CHROME_VERSION = $(CHROME_VERSION)
	@echo CONSUL_VERSION = $(CONSUL_VERSION)
	@echo SERVICE_REGISTRY_VERSION = $(SERVICE_REGISTRY_VERSION)
	@echo PLATFORM = $(PLATFORM)
	
base:
	cd ./Base && docker build $(BUILD_ARGS) -t $(PREFIX)/base:$(ALPINE_VERSION) .

generate_storage:
	cd ./generate/Storage && ./generate.sh Storage $(ALPINE_VERSION) base $(ALPINE_VERSION)

storage: base generate_storage
	cd ./Storage && docker build $(BUILD_ARGS) -t $(PREFIX)/storage:$(ALPINE_VERSION) .

generate_maven:
	cd ./generate/Maven && ./generate.sh Maven $(MAVEN_VERSION) base $(ALPINE_VERSION)

maven: base generate_maven
	cd ./Maven && docker build $(BUILD_ARGS) -t $(PREFIX)/maven:$(MAVEN_VERSION) .

generate_selenium_base:
	cd ./generate/SeleniumBase && ./generate.sh SeleniumBase $(SELENIUM_VERSION) base $(ALPINE_VERSION)

selenium_base: base generate_selenium_base
	cd ./SeleniumBase && docker build $(BUILD_ARGS) -t $(PREFIX)/selenium-base:$(SELENIUM_VERSION) .

generate_selenium_server:
	cd ./generate/SeleniumServer && ./generate.sh SeleniumServer $(SELENIUM_VERSION) selenium-base $(SELENIUM_VERSION)

selenium_server: selenium_base generate_selenium_server
	cd ./SeleniumServer && docker build $(BUILD_ARGS) -t $(PREFIX)/selenium-server:$(SELENIUM_VERSION) .

generate_chrome_browser:
	cd ./generate/ChromeBrowser && ./generate.sh ChromeBrowser $(CHROME_VERSION) selenium-server $(SELENIUM_VERSION)

chrome_browser: selenium_server generate_chrome_browser
	cd ./ChromeBrowser && docker build $(BUILD_ARGS) -t $(PREFIX)/chrome-browser:$(CHROME_VERSION) .

generate_consul:
	cd ./generate/Consul && ./generate.sh Consul $(CONSUL_VERSION) base $(ALPINE_VERSION)

consul: base generate_consul
	cd ./Consul && docker build $(BUILD_ARGS) -t $(PREFIX)/consul:$(CONSUL_VERSION) .

generate_service_registry:
	cd ./generate/ServiceRegistry && ./generate.sh ServiceRegistry $(SERVICE_REGISTRY_VERSION) base $(ALPINE_VERSION)

service_registry: base generate_service_registry
	cd ./ServiceRegistry && docker build $(BUILD_ARGS) -t $(PREFIX)/service-registry:$(SERVICE_REGISTRY_VERSION) .

tag_latest:
	docker tag $(PREFIX)/base:$(ALPINE_VERSION) $(PREFIX)/base:latest
	docker tag $(PREFIX)/storage:$(ALPINE_VERSION) $(PREFIX)/storage:latest
	docker tag $(PREFIX)/maven:$(MAVEN_VERSION) $(PREFIX)/maven:latest
	docker tag $(PREFIX)/selenium-base:$(SELENIUM_VERSION) $(PREFIX)/selenium-base:latest
	docker tag $(PREFIX)/selenium-server:$(SELENIUM_VERSION) $(PREFIX)/selenium-server:latest
	docker tag $(PREFIX)/chrome_browser:$(CHROME_VERSION) $(PREFIX)/chrome_browser:latest
	docker tag $(PREFIX)/consul:$(CONSUL_VERSION) $(PREFIX)/consul:latest
	docker tag $(PREFIX)/service-registry:$(SERVICE_REGISTRY_VERSION) $(PREFIX)/service-registry:latest

release:
	@if ! docker images $(PREFIX)/base | awk '{ print $$2 }' | grep -q -F $(ALPINE_VERSION); then echo "$(PREFIX)/base version $(ALPINE_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/storage | awk '{ print $$2 }' | grep -q -F $(ALPINE_VERSION); then echo "$(PREFIX)/storage version $(ALPINE_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/maven | awk '{ print $$2 }' | grep -q -F $(MAVEN_VERSION); then echo "$(PREFIX)/maven version $(MAVEN_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/selenium-base | awk '{ print $$2 }' | grep -q -F $(SELENIUM_VERSION); then echo "$(PREFIX)/selenium-base version $(SELENIUM_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/selenium-server | awk '{ print $$2 }' | grep -q -F $(SELENIUM_VERSION); then echo "$(PREFIX)/selenium-server version $(SELENIUM_VERSION) is not yet built. Please run 'make build'"; false; fi	
	@if ! docker images $(PREFIX)/chrome_browser | awk '{ print $$2 }' | grep -q -F $(CHROME_VERSION); then echo "$(PREFIX)/chrome_browser version $(CHROME_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/consul | awk '{ print $$2 }' | grep -q -F $(CONSUL_VERSION); then echo "$(PREFIX)/consul version $(CONSUL_VERSION) is not yet built. Please run 'make build'"; false; fi
	@if ! docker images $(PREFIX)/service-registry | awk '{ print $$2 }' | grep -q -F $(SERVICE_REGISTRY_VERSION); then echo "$(PREFIX)/service-registry version $(SERVICE_REGISTRY_VERSION) is not yet built. Please run 'make build'"; false; fi
	docker push $(PREFIX)/base:$(ALPINE_VERSION)
	docker push $(PREFIX)/storage:$(ALPINE_VERSION)
	docker push $(PREFIX)/maven:$(MAVEN_VERSION)
	docker push $(PREFIX)/selenium-base:$(SELENIUM_VERSION)
	docker push $(PREFIX)/selenium-server:$(SELENIUM_VERSION)
	docker push $(PREFIX)/chrome_browser:$(CHROME_VERSION)
	docker push $(PREFIX)/consul:$(CONSUL_VERSION)
	docker push $(PREFIX)/service-registry:$(SERVICE_REGISTRY_VERSION)

.PHONY: \
	all \
	generate \
	build \
	base \
	generate_storage \
	generate_maven \
	generate_selenium_base \
	generate_selenium_server \
	selenium_server \
	generate_chrome_browser \
	chrome_browser \
	generate_consul \
	consul \
	generate_service_registry \
	service-registry \
	tag_latest \
